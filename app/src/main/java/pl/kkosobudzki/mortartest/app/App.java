package pl.kkosobudzki.mortartest.app;

import android.app.Application;
import dagger.ObjectGraph;
import mortar.Mortar;
import mortar.MortarScope;

/**
 * @author Krzysztof Kosobudzki
 */
public class App extends Application {
    private MortarScope mRootScope;

    @Override
    public void onCreate() {
        super.onCreate();

        mRootScope = Mortar.createRootScope(BuildConfig.DEBUG);
    }

    public MortarScope getRootScope() {
        return mRootScope;
    }
}
