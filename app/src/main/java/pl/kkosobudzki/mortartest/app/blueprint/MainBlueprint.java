package pl.kkosobudzki.mortartest.app.blueprint;

import android.os.Bundle;
import mortar.Blueprint;
import mortar.ViewPresenter;
import pl.kkosobudzki.mortartest.app.MainActivity;
import pl.kkosobudzki.mortartest.app.view.MainView;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author Krzysztof Kosobudzki
 */
public class MainBlueprint implements Blueprint {

    @Override
    public String getMortarScopeName() {
        return getClass().getName();
    }

    @Override
    public Object getDaggerModule() {
        return new MainModule();
    }

    @dagger.Module(
        injects = { MainActivity.class, MainView.class }
    )
    public class MainModule {

    }

    @Singleton
    public static class Presenter extends ViewPresenter<MainView> {
        private static final String SERIAL_KEY = "serial";

        private int mSerial = -1;

        @Inject
        Presenter() {

        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (savedInstanceState != null && mSerial == -1) {
                mSerial = savedInstanceState.getInt(SERIAL_KEY);
            }

            getView().show(String.format("Update #%d", mSerial));
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);

            outState.putInt(SERIAL_KEY, mSerial);
        }
    }
}
