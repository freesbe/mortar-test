package pl.kkosobudzki.mortartest.app;

import android.app.Activity;
import android.os.Bundle;
import mortar.Mortar;
import mortar.MortarActivityScope;
import mortar.MortarScope;
import pl.kkosobudzki.mortartest.app.blueprint.MainBlueprint;

public class MainActivity extends Activity {
    private MortarActivityScope mActivityScope;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MortarScope parentScope = ((App) getApplication()).getRootScope();
        mActivityScope = Mortar.requireActivityScope(parentScope, new MainBlueprint());

        Mortar.inject(this, this);

        mActivityScope.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    @Override
    public Object getSystemService(String name) {
        if (Mortar.isScopeSystemService(name)) {
            return mActivityScope;
        }

        return super.getSystemService(name);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mActivityScope.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isFinishing() && mActivityScope != null) {
            MortarScope parentScope = ((App) getApplication()).getRootScope();
            parentScope.destroyChild(mActivityScope);

            mActivityScope = null;
        }
    }
}
