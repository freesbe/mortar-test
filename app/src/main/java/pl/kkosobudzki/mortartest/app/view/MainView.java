package pl.kkosobudzki.mortartest.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import mortar.Mortar;
import pl.kkosobudzki.mortartest.app.R;
import pl.kkosobudzki.mortartest.app.blueprint.MainBlueprint;

import javax.inject.Inject;

/**
 * @author Krzysztof Kosobudzki
 */
public class MainView extends LinearLayout {
    @Inject
    MainBlueprint.Presenter mPresenter;

    @InjectView(R.id.hello_text_view) TextView mHelloTextView;

    public MainView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        Mortar.inject(context, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        ButterKnife.inject(this);

        mPresenter.takeView(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mPresenter.dropView(this);
    }

    public void show(CharSequence str) {
        mHelloTextView.setText(str);
    }
}
